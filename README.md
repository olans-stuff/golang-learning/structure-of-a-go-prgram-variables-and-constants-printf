# Structure
- have to put in package
- fmt is package for print https://pkg.go.dev/fmt
- need a func main() like c++

# Variable
- used to store values
- give a variable a name and reference it everywhere in app
- Makes app more flexible

![Key ](./Images/KeyWords.PNG)

- MUST use all pacakges and variables, otherwise will error
- const: values not allowed to change

# printf
- example how it would look 
```Go
fmt.Printf("Welcome to %v  booking application\n", conferenceName)
```
